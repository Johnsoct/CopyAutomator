const express = require('express');
const parseController = require('../controllers/parseController');
const router = express.Router();

router.get('/', (req, res) => {
  res.sendFile('index.html');
});

router.post('/', parseController.parseResponse);

module.exports = router;
