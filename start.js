const express = require('express');
const path = require('path');
const fs = require('fs');
const bodyparser = require('body-parser');
const axios = require('axios');
const routes = require('./routes/routes');

const app = express();

app.use(express.static(path.join(__dirname, 'views')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

app.use('/', routes);

app.listen('8080');

console.log('Magic happens on port 8080');

exports = module.exports = app;
