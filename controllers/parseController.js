const axios = require('axios');
const puppeteer = require('puppeteer');

const removeTags = (str) => {
  let text = str.trim();
  let removeable = '';
  Array.from(text).forEach(char => {
    // if the removing process has started
    if (removeable) {
      if (char === '>') {
        // build the string to remove from text
        removeable += char;
        // new text after removing string
        const replacedString = text.replace(removeable, '').replace(/\r\n|\r|\n/g, ' - ');
        const trimmedString = replacedString.replace(/^( - )+|( - )+$/g, '');
        text = trimmedString;
        // reset removeable string
        removeable = '';
      } else {
        // build the string to remove from text
        removeable += char;
      }
    }
    // if the removing process hasn't started
    if (!removeable && char === '<') {
      // build the string to remove from text
      removeable += char;
    }
  });
  return text;
}

exports.parseResponse = (req, res) => {


  (async () => {
    // web scraping magic
    const url = req.body.url;
    let html = '';

    // Headless Chrome
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(url, { waitUntil: 'networkidle2' });
    html = Array.from(await page.content());
    await browser.close();

    let text = [];
    const matches = ['<h1', '<h2', '<h3', '<h4', '<p ', '<a ', '<li', '<bu', '<in'];
    const types = [
      {
        tagname: 'header',
        tag: 'h'
      },
      {
        tagname: 'paragraph',
        tag: 'p'
      },
      {
        tagname: 'link',
        tag: 'a'
      },
      {
        tagname: 'list item',
        tag: 'li'
      },
      {
        tagname: 'button',
        tag: 'button'
      }
      //   {
      //     tagname: 'input',
      //     tag: 'input'
      //   }
    ];


    html.forEach((char, index) => {
      // char === 1 character
      // a pair should be 3 chars long. '<' is needed to distinguish from normal text
      const pair = char + html[index + 1] + html[index + 2];
      matches.forEach(match => {
        if (pair === match) {
          const obj = {};
          // store the type of the match
          types.forEach(type => {
            // pair[1] is the tag character after '<'
            if (pair[1] === type.tag) {
              obj.tagname = type.tagname;
              obj.tag = type.tag;
            }
          });
          // store the text of the match
          const endOfBeginTag = html.indexOf('>', index);
          const beginOfEndTag = html.join('').indexOf(`</${obj.tag}`, endOfBeginTag);
          const textContent = html.slice(endOfBeginTag + 1, beginOfEndTag).join('').trim();
          obj.text = textContent;

          text.push(obj);
        }
      });
    });

    // Display results
    const headers = [];
    const paragraphs = [];
    const links = [];
    const listItems = [];
    const buttons = [];

    text.forEach(obj => {
      // headers
      if (obj.tag === 'h') {
        obj.text = removeTags(obj.text);
        headers.push(obj);
      }

      // paragraphs
      if (obj.tag === 'p') {
        obj.text = removeTags(obj.text);
        paragraphs.push(obj);
      }

      // links
      if (obj.tag === 'a') {
        // do not include if <a> wraps a <div>
        if (!(obj.text).includes('div') && !(obj.text).includes('img')) {
          // remove all tags from link's text
          obj.text = removeTags(obj.text);
          links.push(obj);
        }
      }

      // list items
      if (obj.tag === 'li') {
        obj.text = removeTags(obj.text);
        listItems.push(obj);
      }

      // buttons
      if (obj.tag === 'button') {
        obj.text = removeTags(obj.text);
        buttons.push(obj);
      }
    });

    // console.log(headers);
    // console.log(paragraphs);
    console.log(links);
    // console.log(listItems);
    // console.log(buttons);

    res.redirect('/');
  })();

  
};
