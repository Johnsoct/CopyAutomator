/*
 * Competitor Analysis Copy Occurence Counter (CACOC)
 * Import a .CSV with all your competitiors headers, phrases, keywords, body text, and export a .CSV with the # of occurences of each.
 * 
 * Functionality:
 * exportableData WILL hold the data to be exported into a .CSV. It will NOT be properly formatted for a .CSV until exportData().
 * 
 * 1. handleImport() handles the event of importing using an input w/ FileReader().
 * 2. handleImport() calls prepareData() to create an array of all phrases and keywords in the .CSV upload data.
 * 3. handleImport() calls sortKeywordPhrases() on the result of prepareData() to sort the array into two arrays, keywords and phrases, based on whether the index contains spaces (phrase) or not (keyword).
 * 4. handleImport() calls common on the sorted data from sortKeywordPhrases() to count the occurence of each keyword or phrase by iterating through each keyword/phrase then iterating through each already counted keyword/phrase within each individual keyword/phrase iteration. Next, the counted keywords/phrases/ are sorted in descending order by counts. Finally, the counted keywords and phrases are combined into an array and set as exportableData;
 * 5. After common(), the newly formatted, counted, and sorted data is immediately exported by calling exportData().
 */

import axios from 'axios';

let exportableData = null;

// ajax requesting a url's HTML
const requestPageHTML = url => {
  console.log('copied');

  axios.get(`http://localhost:8080?url=${url}`).then((response) => {
    console.log(response.data);
  }).catch((err) => {
    console.log(err);
  });
}

// parse HTML and look for all h1, h2, h3, h4, and p elements into array

// send parsed HTML into prepareData()


const prepareData = data => {
  const lines = Array.from(data.split(/[,"/\n/]+/));
  // trim all white space
  const trimmedLines = lines.map(line => line.trim());
  // filter out any "" items
  const filterForAnyValue = line => {
    line.trim();
    return line !== ""; // ""
  }
  const minimizedData = trimmedLines.filter(filterForAnyValue);

  return minimizedData;
}

const sortKeywordsPhrases = data => {
  const keywords = [];
  const phrases = [];
  // iterate over each index of data
  data.forEach(item => {
    // store the first bit in lower case
    const firstBit = item.toLowerCase();
    // if contains spaces, store as phrases
    // if one word or hypenated, store as keywords
    if (firstBit.includes(' ')) {
      phrases.push(firstBit);
    } else {
      keywords.push(firstBit);
    }
  });
  return {
    keywords,
    phrases
  };
}

const common = keywordsAndPhrases => {
  const keywords = keywordsAndPhrases.keywords;
  const phrases = keywordsAndPhrases.phrases;
  const keywordsCounted = [];
  const phrasesCounted = [];
  let combinedCounted = [];
  keywords.forEach(keyword => {
    // if keyword has already been counted
    let truthiness = false;
    keywordsCounted.forEach(counted => {
      if (!counted) keywordsCounted.push({ keyword, count: 1 });
      // track if keyword is in keywordsCounted
      if (counted.keyword === keyword) {
        // keyword matches a keyword in keywordsCounted
        truthiness = true;
        // increase keyword count
        counted.count++;
      }
    });
    // if the keyword didn't exist in keywordsCount, add it
    if (!truthiness) {
      keywordsCounted.push({
        keyword: keyword,
        count: 1
      });
    }
  });
  phrases.forEach(phrase => {
    // if phrase has already been counted
    let truthiness = false;
    phrasesCounted.forEach(counted => {
      if (!counted) phrasesCounted.push({ keyword: phrase, count: 1 });
      // track if phrase is in phrasesCounted
      if (counted.phrase === phrase) {
        // phrase matches a phrase in phrasesCounted
        truthiness = true;
        // increase phrase count
        counted.count++;
      }
    });
    // if the phrase didn't exist in phrasesCount, add it
    if (!truthiness) {
      phrasesCounted.push({
        keyword: phrase,
        count: 1
      });
    }
  });

  // sort by # of occurences
  keywordsCounted.sort((a, b) => b.count - a.count);
  phrasesCounted.sort((a, b) => b.count - a.count);

  combinedCounted = keywordsCounted.concat(phrasesCounted);
  exportableData = combinedCounted;

  return combinedCounted;
}

const exportData = () => {
  // format into csv
  let csvContent = "data:text/csv;charset=utf-8,";
  exportableData.forEach(obj => {
    const row = `${obj.keyword},${obj.count},` + "\r\n";
    csvContent += row;
  });
  const encodedURI = encodeURI(csvContent);
  // export using a hidden link w/ custom file name
  const link = document.createElement('a');
  link.setAttribute('href', encodedURI);
  link.setAttribute('download', 'counted_occurences.csv');
  link.style.visibility = 'hidden';
  document.body.appendChild(link);
  link.click();
}

const handleImport = (files) => {
  const file = files[0];
  const reader = new FileReader();
  reader.onload = () => {
    const searchableData = prepareData(reader.result);
    // console.log(searchableData);
    const sortedData = sortKeywordsPhrases(searchableData);
    // console.log(sortedData);
    const commonData = common(sortedData);
    exportData();
  }
  reader.readAsText(file);
}

